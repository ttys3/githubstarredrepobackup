package main

import (
	"fmt"

	"github.com/globalsign/mgo"
)

//options
type MongoOption struct {
	User   string
	Passwd string
	Host   string
	Port   string
	DbName string
	Otions string
}

type StarDb struct {
	Option *MongoOption
	Sess   *mgo.Session
}

//New *StarDb
func New(User, Passwd, Host, Port, DbName, options string) *StarDb {
	opt := &MongoOption{
		User:   User,
		Passwd: Passwd,
		Host:   Host,
		Port:   Port,
		DbName: DbName,
		Otions: options,
	}
	mongoDbURL := fmt.Sprintf("mongodb://%s:%s@%s:%s/%s?%s", opt.User, opt.Passwd, opt.Host, opt.Port, opt.DbName, opt.Otions)
	session, err := mgo.Dial(mongoDbURL)
	if err != nil {
		panic(err)
	}
	cdb := &StarDb{
		Option: opt,
		Sess:   session,
	}
	return cdb
}
